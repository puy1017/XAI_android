# XAI 使用说明文档
# 1.Andriod_SDK 简介
    Android SDK以Jar包形式提供，封装了物联网平台XAI各种数据接口，方便android端开发者轻松实现与物联网平台的对接。
# 2.下载SDK
[XAI SDK](https://gitee.com/puy1017/XAI_android/blob/master/SDK/XAI_SDK_JAVA_APP.jar)
# 3.导入SDK
    以下通过android studio3.0.1开发工具作为范例说明

	将SDK导入至工程下libs文件夹中并  add as library...

![](https://gitee.com/puy1017/XAI_android/raw/master/image/c790bf33-28ca-442b-b1f2-2bd272030f79.png)
<pre style="background-color:#ffffff;color:#000000;font-family:'Consolas';font-size:15.0pt;">dependencies {
   ....
  implementation files('libs/XAI_SDK_JAVA_APP.jar')
}</pre>

# 4.导入证书
    将bks格式证书放至raw文件夹下
![](https://gitee.com/puy1017/XAI_android/raw/master/image/9663823e-e47f-4fba-bf6a-cd306a67a5fc.png)
# 5.参数配置
	在AndroidManifest.xml中添加权限：
	<uses-permission android:name="android.permission.INTERNET"/>
	<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"/>
	<uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE"/>

	注：Android 6.0及以上需要动态申请WRITE_EXTERNAL_STORAGE权限后才能正常使用。

# 6.账号注册
	1.获取短信验证码
	/**
	 * 通过 restFUL API请求短信验证码
	 * 
	 * @param host
	 *            web服务器地址
	 * @param port
	 *            web服务器端口
	 * @param mobile
	 *            手机号
	 * @param sslConnection
	 *            是否为https连接
	 * @return 布尔值
	 */
	
	public static boolean requestSMSVerifyCode(String host, int port, String mobile, boolean sslConnection)
	2.注册帐号
	/**
	 * 通过restFUL API 注册手机用户
	 * 
	 * @param host
	 *            web服务器地址
	 * @param port
	 *            web服务器端口号
	 * @param mobile
	 *            手机号
	 * @param password
	 *            密码
	 * @param sms_code
	 *            短信验证码
	 * @param sslConnection
	 *            是否使为https连接
	 * @return http返回数据
	 */
	public static String registerUser(String host, int port, String mobile, String password, String sms_code,
	        boolean sslConnection)


注：发送短信和注册帐号属于http网络请求，应当放在非主线程中调用

可参考demo事例[RegisterUserActivity](https://gitee.com/puy1017/XAI_android/blob/master/app/src/main/java/com/puy/xai/RegisterUserActivity.java)



# 7.XAI初始化

	/**
	 * 
	 * @param mobile
	 *            帐号
	 * @param callback
	 *            消息回调接口
	 */
    XAI(String mobile, XAICallback callback);

	/**
	 * 建立网络连接（设备登录）
	 * 
	 * @param password
	 *            登录密码
	 * @param host
	 *            服务器地址
	 * @param port
	 *            服务器端口
	 * @param caFile
	 *            证书文件流
	 * @param caPassword
	 *            证书密码
	 * @param dirCache
	 *            缓存目录
	 * @param cleansession
	 *            是否全新连接
	 * @return ERRNO
	 */
	XAI.connect(String password, String host, int port, InputStream caFile, String caPassword, String dirCache, boolean cleansession);

## 示例 ##

		XAICallback callback = new XAICallback() {
            @Override
            public void messageStatus(int from_apsn, long from_luid, int trigger_apsn, long trigger_luid, long timestamp,int id, ArrayList<TLV> tlvs) {
                
            }

            @Override
            public void messageStatusHistory(int from_apsn, long from_luid, int trigger_apsn, long trigger_luid, long timestamp,int id, ArrayList<TLV> tlvs) {

            }

            @Override
            public ERRNO messageCMD(int from_apsn, long from_luid, long timestamp, int id, ArrayList<TLV> tlvs, ArrayList<TLV> ret_vals) {
                return null;
            }

            @Override
            public void messageIM(int from_apsn, long from_luid, long timestamp, ArrayList<TLV> tlvs) {

            }

            @Override
            public void deviceRemoved(int apsn, long luid) {

            }

            @Override
            public void deviceDetected(int apsn, long luid, int type, int ver, String des) {

            }

            @Override
            public void linkRuleDetected(int apsn, int id, String name, boolean active, ArrayList<LINK_RULE> condition,ArrayList<LINK_RULE> action) {

            }

            @Override
            public void linkRuleRemoved(int apsn, int id) {

            }

            @Override
            public void pushParaDetected(int vendor, String para) {

            }

            @Override
            public void pushRuleDetected(int apsn, boolean enable, String registerID, ArrayList<PushRule> configs) {

            }

            @Override
            public void adminDetected(int apsn, long luid_admin) {

            }
        };	

	  	XAI xai = new XAI("推荐使用手机帐号", callback);
		//获取raw文件夹下bks证书
        InputStream ca_file_is = context.getResources().openRawResource(R.raw.ca);
        if (xai.connect("登录密码", "服务器地址", "服务器端口", ca_file_is, "证书密码", Environment.getExternalStorageDirectory() + "/GatewayShare", true) != ERRNO.OK) {
            LogFile.Write("failed to connect to server\n");
        }

注：可参考demo事例中[Mobile类](https://gitee.com/puy1017/XAI_android/blob/master/app/src/main/java/example/Mobile.java)


# XAI  方法说明

	/**
	 * 
	 * @param host
	 *            云服务器地址
	 * @param port
	 *            云服务器TCP端口
	 * @param callback
	 *            消息回调接口
	 */
	public XAI(int apsn, long luid, XAICallback callback)

	/**
	 * 
	 * @param mobile
	 *            用户帐号
	 * @param callback
	 *            消息回调接口
	 */
	public XAI(String mobile, XAICallback callback)

	/**
	 * 建立网络连接（设备登录）
	 * 
	 * @param password
	 *            登录密码
	 * @param host
	 *            服务器地址
	 * @param port
	 *            服务器端口
	 * @param caFile
	 *            证书文件流
	 * @param caPassword
	 *            证书密码
	 * @param dirCache
	 *            缓存目录
	 * @param cleansession
	 *            是否全新连接
	 * @return ERRNO
	 */
	public ERRNO connect(String password, String host, int port, InputStream caFile, String caPassword, String dirCache,
			boolean cleansession)

	/**
	 * 建立网络连接（设备登录）
	 * 
	 * @param password
	 *            登录密码
	 * @param host
	 *            服务器地址
	 * @param port
	 *            服务器端口
	 * @param caFile
	 *            证书路径
	 * @param caPassword
	 *            证书密码
	 * @param dirCache
	 *            缓存目录
	 * @param cleansession
	 *            是否全新连接
	 * @return ERRNO
	 */
	public ERRNO connect(String password, String host, int port, String caFile, String caPassword, String dirCache,
	        boolean cleansession)

	/**
	 * 链接注销
	 */
	public void close()

	/**
	 * 发送设备的设备信息
	 * 
	 * @param id
	 *            状态编号
	 * @param data_type
	 *            数据类型
	 * @param data
	 *            数据
	 * @return ERRNO
	 */
	public ERRNO sendDev(int from_apsn, long from_luid, int to_apsn, long to_luid, int type, int ver,
	        boolean will_sleep, String descritpion)

	/**
	 * 发送本机设备信息
	 * 
	 * @param type
	 *            设备类型
	 * @param ver
	 *            软件版本
	 * @param will_sleep
	 *            是否休眠
	 * @param descritpion
	 *            设备文字描述
	 * @return ERRNO
	 */
	public ERRNO sendMyDev(int type, int ver, boolean will_sleep, String descritpion)

	/**
	 * 发送本机状态信息
	 * 
	 * @param id
	 *            状态编号
	 * @param tlvs
	 *            状态数据集
	 * @return ERRNO
	 */
	public ERRNO sendMyStatus(int id, ArrayList<TLV> tlvs)

	/**
	 * 发送本机状态信息
	 * 
	 * @param id
	 *            状态编号
	 * @param tlv
	 *            状态数据
	 * @return ERRNO
	 */
	public ERRNO sendMyStatus(int id, TLV tlv)

	/**
	 * 发送本机连接状态信息
	 * 
	 * @param online
	 *            是否在线
	 * @return ERRNO
	 */
	public ERRNO sendMyDeviceConnection(boolean online)

	/**
	 * 发送本机名字
	 * 
	 * @param name
	 *            名字
	 * @return ERRNO
	 */
	public ERRNO sendMyDeviceName(String name)

	/**
	 * 发送本机设备电量信息
	 * 
	 * @param power
	 *            电量（0-100）
	 * @return ERRNO
	 */
	public ERRNO sendMyDevicePower(int power)

	/**
	 * 发送手机推送配置 如果需要使用此功能请加入交流群（768813308）进行询问
	 * @param apsnGateway
	 * @param pushDes
	 * @return
	 */
	public ERRNO sendMyPushConfig(int apsnGateway, PushDes pushDes)

	/**
	 * 发送设备的状态信息
	 * 
	 * @param id
	 *            状态编号
	 * @param tlvs
	 *            数据集
	 * @return ERRNO
	 */
	public ERRNO sendStatus(int from_apsn, long from_luid, int to_apsn, long to_luid, int id, TLV tlv)

	/**
	 * 发送设备的状态信息
	 * 
	 * @param id
	 *            状态编号
	 * @param tlvs
	 *            数据集
	 * @return ERRNO
	 */
	public ERRNO sendStatus(int from_apsn, long from_luid, int to_apsn, long to_luid, int id, ArrayList<TLV> tlvs)

	/**
	 * 发送设备的状态信息
	 * 
	 * @param id
	 *            状态编号
	 * @param tlvs
	 *            数据集
	 * @return ERRNO
	 */
	public ERRNO sendStatus(int from_apsn, long from_luid, int to_apsn, long to_luid, int trigger_apsn,
	        long trigger_luid, int id, TLV tlv)

	/**
	 * 发送设备的状态信息
	 * 
	 * @param id
	 *            状态编号
	 * @param tlvs
	 *            数据集
	 * @return ERRNO
	 */
	public ERRNO sendStatus(int from_apsn, long from_luid, int to_apsn, long to_luid, int trigger_apsn,
	        long trigger_luid, int id, ArrayList<TLV> tlvs)

	/**
	 * 发送控制命令到指定用户、设备
	 * 
	 * @param to_apsn
	 *            接收方所属的网关编号
	 * @param to_luid
	 *            接收方设备编号号
	 * @param id
	 *            控制命令编号
	 * @param tlv
	 *            数据
	 * @param timeout_ms
	 *            ACK超时 毫秒
	 * @return ERRNO
	 */
	public ERRNO sendCMD(int to_apsn, long to_luid, int id, TLV tlv, int timeout_ms)

	/**
	 * 发送控制命令到指定用户、设备
	 * 
	 * @param to_apsn
	 *            接收方所属的网关编号
	 * @param to_luid
	 *            接收方设备编号号
	 * @param id
	 *            控制命令编号
	 * @param tlvs
	 *            数据集
	 * @param timeout_ms
	 *            ACK超时 毫秒
	 * @return ERRNO
	 */
	public ERRNO sendCMD(int to_apsn, long to_luid, int id, ArrayList<TLV> tlvs, int timeout_ms)

	/**
	 * 发送控制命令到指定用户、设备
	 * 
	 * @param to_apsn
	 *            接收方所属的网关编号
	 * @param to_luid
	 *            接收方设备编号号
	 * @param id
	 *            控制命令编号
	 * @param tlv
	 *            数据
	 * @param ret_tlvs
	 *            返回数据集
	 * @param timeout_ms
	 *            ACK超时，毫秒
	 * @return ERRNO
	 */
	public ERRNO sendCMD(int to_apsn, long to_luid, int id, TLV tlv, ArrayList<TLV> ret_tlvs, int timeout_ms)

	/**
	 * 发送控制命令到指定用户、设备
	 * 
	 * @param to_apsn
	 *            接收方所属的网关编号
	 * @param to_luid
	 *            接收方设备编号号
	 * @param id
	 *            控制命令编号
	 * @param tlvs
	 *            数据集
	 * @param ret_tlvs
	 *            返回数据集
	 * @param timeout_ms
	 *            ACK超时，毫秒
	 * @return ERRNO
	 */
	public ERRNO sendCMD(int to_apsn, long to_luid, int id, ArrayList<TLV> tlvs, ArrayList<TLV> ret_tlvs,
	        int timeout_ms)

	/**
	 * 发送控制命令到指定用户、设备
	 * 
	 * @param from_apsn
	 *            发送方所属的网关编号
	 * @param from_luid
	 *            发送方设备编号
	 * @param to_apsn
	 *            接收方所属的网关编号
	 * @param to_luid
	 *            接收方设备编号
	 * @param id
	 *            控制命令编号
	 * @param tlvs
	 *            数据集
	 * @return ERRNO
	 */
	public ERRNO sendCMD(int from_apsn, long from_luid, int to_apsn, long to_luid, int id, TLV tlv, int timeout_ms)

	/**
	 * 发送控制命令到指定用户、设备
	 * 
	 * @param from_apsn
	 *            发送方所属的网关编号
	 * @param from_luid
	 *            发送方设备编号
	 * @param to_apsn
	 *            接收方所属的网关编号
	 * @param to_luid
	 *            接收方设备编号
	 * @param id
	 *            控制命令编号
	 * @param tlvs
	 *            数据集
	 * @return ERRNO
	 */
	public ERRNO sendCMD(int from_apsn, long from_luid, int to_apsn, long to_luid, int id, ArrayList<TLV> tlvs,
	        int timeout_ms)

	/**
	 * 发送聊天消息到指定用户、设备
	 * 
	 * @param to_apsn
	 *            指定用户所属的网关编号
	 * @param to_luid
	 *            指定用户、设备的设备编号
	 * @param tlv
	 *            数据
	 * @return ERRNO
	 */
	public ERRNO sendIM(int to_apsn, long to_luid, TLV tlv)

	/**
	 * 发送聊天消息到指定用户、设备
	 * 
	 * @param to_apsn
	 *            指定用户所属的网关编号
	 * @param to_luid
	 *            指定用户、设备的设备编号
	 * @param tlvs
	 *            数据集
	 * @return ERRNO
	 */
	public ERRNO sendIM(int to_apsn, long to_luid, ArrayList<TLV> tlvs)

	/**
	 * 发送聊天消息到指定用户、设备
	 * 
	 * @param apsn
	 *            指定用户所属的网关编号
	 * @param luid
	 *            指定用户、设备的设备编号
	 * @param data_type
	 *            数据类型
	 * @param tlvs
	 *            数据集
	 * @return ERRNO
	 */
	public ERRNO sendIM(int from_apsn, long from_luid, int to_apsn, long to_luid, TLV tlv)

	/**
	 * 发送聊天消息到指定用户、设备
	 * 
	 * @param apsn
	 *            指定用户所属的网关编号
	 * @param luid
	 *            指定用户、设备的设备编号
	 * @param data_type
	 *            数据类型
	 * @param tlvs
	 *            数据集
	 * @return ERRNO
	 */
	public ERRNO sendIM(int from_apsn, long from_luid, int to_apsn, long to_luid, ArrayList<TLV> tlvs)

	/**
	 * 设置推送标记
	 * 
	 * @param apsnGateway
	 *            推送标志所属网关
	 * @param registerID
	 *            JPUSH注册ID
	 * @param enable
	 *            是否开启推送
	 * @param configs
	 *            推送配置数据
	 * @return ERRNO
	 */
	public ERRNO sendPushRule(int apsnGateway, boolean enable, ArrayList<PushRule> configs)
	
	/**
	 * 获取指定设备的历史消息
	 * 
	 * @param apsn
	 *            指定设备所属网关编号
	 * @param luid
	 *            指定设备的设备编号
	 * @return ERRNO
	 */
	public ERRNO enableHistory(int apsn, long luid)

	/**
	 * 添加设备到指定网关
	 * 
	 * @param apsn
	 *            设备所属网关
	 * @param luid
	 *            设备编号
	 * @return ERRNO
	 */
	public ERRNO addDevice(int apsn, long luid)

	/**
	 * 从指定网关删除设备
	 * 
	 * @param apsn
	 *            设备所属网关
	 * @param luid
	 *            设备编号
	 * @return ERRNO
	 */
	public ERRNO removeDdevice(int apsn, long luid)

	/**
	 * 通过 restFUL API请求短信验证码
	 * 
	 * @param host
	 *            web服务器地址
	 * @param port
	 *            web服务器端口
	 * @param mobile
	 *            手机号
	 * @param sslConnection
	 *            是否为https连接
	 * @return 布尔值
	 */
	
	public static boolean requestSMSVerifyCode(String host, int port, String mobile, boolean sslConnection)

	/**
	 * 通过restFUL API 注册手机用户
	 * 
	 * @param host
	 *            web服务器地址
	 * @param port
	 *            web服务器端口号
	 * @param mobile
	 *            手机号
	 * @param password
	 *            密码
	 * @param sms_code
	 *            短信验证码
	 * @param sslConnection
	 *            是否使为https连接
	 * @return http返回数据
	 */
	public static String registerUser(String host, int port, String mobile, String password, String sms_code,
	        boolean sslConnection)

	/**
	 * 通过restFUL API 重置手机密码
	 * 
	 * @param host
	 *            web服务器地址
	 * @param port
	 *            web服务器端口
	 * @param mobile
	 *            手机号
	 * @param password
	 *            密码
	 * @param sms_code
	 *            短信验证码
	 * @param sslConnection
	 *            是否使为https连接
	 * @return
	 */
	public static String resetPassword(String host, int port, String mobile, String password, String sms_code,
	        boolean sslConnection)

	/**
	 * 通过restFUL API更改手机密码
	 * 
	 * @param host
	 *            web服务器地址
	 * @param port
	 *            web服务器端口
	 * @param mobile
	 *            手机号
	 * @param password_old
	 *            原始密码
	 * @param password_new
	 *            新密码
	 * @param sms_code
	 *            短信验证码
	 * @param sslConnection
	 *            是否为https连接
	 * @return http返回数据
	 */
	public static String changePassword(String host, int port, String mobile, String password_old, String password_new,
	        String sms_code, boolean sslConnection)

	/**
	 * 添加联动到指定网关
	 * 
	 * @param apsn
	 *            待添加到的网关编号
	 * @param name
	 *            联动名字
	 * @param condition
	 *            联动条件组
	 * @param action
	 *            联动动作组
	 * @return ERRNO
	 */
	public ERRNO addLink(int apsn, String name, ArrayList<LINK_RULE> condition, ArrayList<LINK_RULE> action)

	/**
	 * 修改联动到指定网关
	 * 
	 * @param apsn
	 *            待修改联动的网关编号
	 * @param name
	 *            联动名字
	 * @param condition
	 *            联动条件组
	 * @param action
	 *            联动动作组
	 * @return ERRNO
	 */
	public ERRNO modifyLink(int apsn, int id, String name, ArrayList<LINK_RULE> condition, ArrayList<LINK_RULE> action)

	/**
	 * 设置指定网关上的联动状态
	 * 
	 * @param apsn
	 *            联动所在的网关编号
	 * @param id
	 *            联动编号
	 * @param active
	 *            启用还是禁用
	 * @return ERRNO
	 */
	public ERRNO enableLink(int apsn, int id, boolean active)

	/**
	 * 删除指定网关上的联动
	 * 
	 * @param apsn
	 *            联动所在的网关编号
	 * @param id
	 *            联动编号
	 * @return ERRNO
	 */
	public ERRNO removeLink(int apsn, int id)

	/**
	 * 运行指定网关上的联动
	 * 
	 * @param apsn
	 *            联动所在的网关编号
	 * @param id
	 *            联动编号
	 * @return ERRNO
	 */
	public ERRNO runLink(int apsn, int id)

	/**
	 * 设置网关是否开启白名单模式
	 * 
	 * @param apsn
	 *            要设置白名单模式的网关编号
	 * @param enable
	 *            是否开启白名单
	 * @return ERRNO
	 */
	public ERRNO enableWhiteList(int apsn, boolean enable)

	/**
	 * 检查网络延迟
	 * 
	 * @return 网络延迟，单位ms
	 */
	public int detectNetworkDelay_MS()

	/**
	 * 服务器是否已连接
	 * 
	 * @return 连接状态
	 */
	public boolean isConnected()

	/**
	 * 通知云网关将请求设备绑定到目标设备
	 * 
	 * @param apsn_request
	 *            请求方所属网关编号
	 * @param luid_request
	 *            请求方设备编号
	 * @param apsn_target
	 *            目标设备所属网关编号
	 * @param luid_target
	 *            目标设备编号
	 * @param flag
	 *            访问权限标志
	 * @return ERRNO
	 */
	public ERRNO allowBind(int apsn_request, long luid_request, int apsn_target, long luid_target, int flag)


	/**
	 * 将当前登录手机号绑定到指定设备
	 * 
	 * @param apsn
	 *            待绑定的设备所属网关编号
	 * @param luid
	 *            待绑定的设备编号
	 * @return
	 */
	public ERRNO bindDevice(int apsn, long luid)


	/**
	 * 将当前登录手机号绑定到指定网关
	 * 
	 * @param apsnGateway
	 *            待绑定的网关编号
	 * @return ERRNO
	 */
	public ERRNO bindGateway(int apsnGateway)


	/**
	 * 解除当前登录手机号对设备的绑定
	 * 
	 * @param apsnGateway
	 *            要解除绑定的网关编号
	 * @return ERRNO
	 */
	/**
	 * 解除当前登录手机号对设备的绑定
	 * 
	 * @param apsn
	 *            待解绑的设备所属网关编号
	 * @param luid
	 *            待解绑的设备编号
	 * @return
	 */
	public ERRNO unbindDevice(int apsn, long luid)


	/**
	 * 解除当前登录手机号对网关的绑定
	 * 
	 * @param apsnGateway
	 *            要解除绑定的网关编号
	 * @return ERRNO
	 */
	public ERRNO unbindGateway(int apsnGateway)

	/**
	 * 搜索局域网内的网关
	 * 
	 * @param timewait_ms
	 *            超时时间(毫秒)
	 * @return 网关IP信息列表
	 */
	public static ArrayList<GATEWAY_DES> searchGatewayLocal(int timewait_ms)

	/**
	 * 搜索局域网内的网关
	 * 
	 * @param callback
	 *            搜索网关异步回调函数
	 * @return 网关IP信息列表
	 */
	public static void searchGatewayAtLocal(GatewaySearchCallback callback)

	/**
	 * 局域网批量绑定网关
	 * 
	 * @param luid_mobile
	 *            绑定者的LUID
	 * @param gateway_list
	 *            要绑定的网关信息列表
	 * @return 成功绑定的数量
	 */
	public static int bindGatewayAtLocal(long luid_mobile, ArrayList<String> gateway_list)

	/**
	 * 局域网绑定网关
	 * 
	 * @param luid_mobile
	 *            绑定者的LUID
	 * @param gateway_ip
	 *            要绑定的网关IP
	 * @param apsn_gateway
	 *            要绑定的网关编号
	 * @param name
	 *            待设定的网关名字
	 * @return boolean值
	 */
	public static boolean bindGatewayAtLocal(long luid_mobile, String gateway_ip, int apsn_gateway, String name)

## ERRNO ##
	/**
	 * 0:正常
	 */
	OK((short) 0),

	/**
	 * 1 :操作没有导致任何变化
	 */
	UNCHANGE((short) 1),

	/**
	 * 2:通用错误
	 */
	GENERAL_ERROR((short) 2),

	/**
	 * 3:无权限
	 */
	NO_PRIV((short) 3),

	/**
	 * 4:用户名已存在
	 */
	NAME_EXISTED((short) 4),

	/**
	 * 5:用户名无效
	 */
	NAME_INVALID((short) 5),

	/**
	 * 6:户不存在
	 */
	NAME_NONE_EXISTED((short) 6),

	/**
	 * 7:设备编号已存
	 */
	LUID_EXISTED((short) 7),

	/**
	 * 8:在设备编号无效
	 */
	LUID_INVALID((short) 8),

	/**
	 * 9:设备编号不存在
	 */
	LUID_NOT_EXISTED((short) 9),

	/**
	 * 10:密码错误
	 */
	PWD_INCORRECT((short) 10),

	/**
	 * 11:设备不存在
	 */
	DEVICE_NONE_EXISTED((short) 11),

	/**
	 * 12:设备离线
	 */
	DEVICE_OFFLINE((short) 12),

	/**
	 * 13:参数错误
	 */
	ARG_INCORRECT((short) 13),

	/**
	 * 14:电量低
	 */
	LOW_POWER((short) 14),

	/**
	 * 15:内存不足
	 */
	MALLOC_ERROR((short) 15),

	/**
	 * 16:内存不足
	 */
	TIMEOUT((short) 16),

	/**
	 * 28:密码不正确
	 */
	PWD_INVALID((short) 28),

	/**
	 * 29:联动已存在
	 */
	LINK_EXISTED((short) 29),

	/**
	 * 30:联动不存在
	 */
	LINK_NOT_EXISTED((short) 30),

	/**
	 * 31:TOKEN不存在
	 */
	TOKEN_NOT_EXIST((short) 31),

	/**
	 * 32:需要重试
	 */
	NEED_RETRY((short) 32),

	/**
	 * 33:TOKEN不正确
	 */
	TOKEN_INCORRECT((short) 33),

	/**
	 * 34:有新固件
	 */
	FIRMWARE_NOT_LATEST((short) 34),

	/**
	 * 35:联动即将失效
	 */
	LINK_WILL_DEACTIVE((short) 35),

	/**
	 * 36:OTA升级完成
	 */
	OTA_FINISHED((short) 36),

	/**
	 * 37:OTA请求下一个数据块
	 */
	OTA_NEXT((short) 37),

	/**
	 * 136:IP不正确
	 */
	IP_FAILED((short) 136),

	/**
	 * 137:网络错误
	 */
	NETWORK_FAILED((short) 137),

	/**
	 * 139:连接丢失
	 */
	CONNECTON_LOST((short) 139);

	/**
	 * 取错误号
	 * 
	 * @return 错误码
	 */
	public int getValue()

	/**
	 * Interger 转 ERRNO
	 * 
	 * @param err
	 *            错误码
	 * @return 错误码
	 */
	public static ERRNO valueOf(int err)

## Convertor 工具类 ##
	/**
	 * int转byte数组(大端)
	 * 
	 * @param integer
	 *            int数据
	 * @return 字节数组
	 */
	public static byte[] intToByteArray(int integer)

	/**
	 * 字符串转byte数组，UTF8编码
	 * 
	 * @param val
	 *            字符串
	 * @return 字节数组
	 */
	public static byte[] stringToByteArray(String val)

	/**
	 * byte转byte数组
	 * 
	 * @param val
	 *            字节数据
	 * @return 字节数组
	 */
	public static byte[] byteToByteArray(byte val)

	/**
	 * 布尔值转byte数组
	 * 
	 * @param val
	 *            布尔数据
	 * @return 字节数组
	 */
	public static byte[] booleanToByteArray(boolean val)

	/**
	 * byte数组转int
	 * 
	 * @param b
	 *            byte数组
	 * @param offset
	 *            偏移量
	 * @return int int数据
	 */
	public static int byteArrayToInt(byte[] b, int offset)

	/**
	 * byte数组转byte
	 * 
	 * @param b
	 *            byte数组
	 * @param offset
	 *            偏移量
	 * @return byte byte数据
	 */
	public static byte byteArrayToByte(byte[] b, int offset)

	/**
	 * byte数组转布尔值
	 * 
	 * @param b
	 *            byte数组
	 * @param offset
	 *            偏移量
	 * @return 布尔值
	 */
	public static boolean byteArrayToBoolean(byte[] b, int offset)

	/**
	 * byte数组转long
	 * 
	 * @param b
	 *            byte数组
	 * @param offset
	 *            偏移量
	 * @return long long数据
	 */
	public static long byteArrayToLong(byte[] b, int offset)

	/**
	 * Short转byte数组
	 * 
	 * @param n
	 *            short数据
	 * @return 字节数组
	 */
	public static byte[] shortToByteArray(short n)


	/**
	 * int转short
	 * 
	 * @param n
	 *            int数据
	 * @return short
	 */
	public static short intToShort(int n)

	/**
	 * byte数组转short
	 * 
	 * @param b
	 *            byte数组
	 * @param offset
	 *            偏移量
	 * @return short
	 */
	public static short byteArrayToShort(byte[] b, int offset)

	/**
	 * ip字符串(192.168.0.1)转int
	 * 
	 * @param ip
	 *            ip地址
	 * @return int int数据
	 */
	public static int stringIPToInt(String ip)

	/**
	 * ip数字转字符串"192.168.0.1"
	 * 
	 * @param ip
	 *            ip整形数据
	 * @return ip字符串地址
	 */
	public static String ipToString(int ip)

	/**
	 * long转byte数组
	 * 
	 * @param x
	 *            long数据
	 * @return 字节数组
	 */
	public static byte[] longToByteArray(long x)

	/**
	 * object转byte数组
	 * 
	 * @param object
	 *            Object对象
	 * @return 字节数组
	 */
	public static byte[] objectToByteArray(Object object)

	/**
	 * byte数组转object
	 * 
	 * @param bytes
	 *            byte数组
	 * @return object
	 */
	public static Object byteArrayToObject(byte[] bytes)

	/**
	 * byte数组转HEX字符串
	 * 
	 * @param bytes
	 *            byte数组
	 * @return HEX字符串
	 */
	public static String byteArray2HexString(byte[] bytes)

	/**
	 * HEX字符串转数组
	 * 
	 * @param hexString
	 *            HEX字符创
	 * @return 字节数组
	 */
	public static byte[] hexStringToByte(String hexString)

## DATA_TYPE ##

	/**
	 * ASCII编码的字符串
	 */
	ASCII_TEXT
	/**
	 * 图片
	 */
	BIN_IMAGE
	/**
	 * 文件
	 */
	BIN_FILE
	/**
	 * GPS位置,(double,double)
	 */
	BIN_LOCATION
	/**
	 * 网络链接
	 */
	ASCII_URL
	/**
	 * 角度数据(float)
	 */
	BIN_ANGLE
	/**
	 * 百分比
	 */
	BIN_PERCENT
	/**
	 * 日期
	 */
	int BIN_DATE
	/**
	 * 网页HTML5数据
	 */
	ASCII_HTML5
	/**
	 * 红外遥控数据
	 */
	BIN_IR
	/**
	 * MQTT TOPIC数据
	 */
	ASCII_TOPIC
	/**
	 * 布尔数据
	 */
	BIN_BOOL
	/**
	 * 有符号整型(8位,16位,32位,64位)
	 */
	BIN_DIGITAL_SIGN
	/**
	 * 无符号整型(8位,16位,32位,64位)
	 */
	BIN_DIGITAL_UNSIGN
	/**
	 * 网关编号
	 */
	BIN_APSN
	/**
	 * 设备编号
	 */
	BIN_LUID
	/**
	 * 设备编号
	 */
	BIN_MAC
	/**
	 * 延时(秒，毫秒等，用户自定单位)
	 */
	BIN_DELAY
	/**
	 * 按钮控件
	 */
	BIN_BUTTON
	/**
	 * 原始报文
	 */
	BIN_PKT
	/**
	 * 联动条件数据
	 */
	BIN_LINK_TRIGGER
	/**
	 * 联动动作数据
	 */
	BIN_LINK_ACTION
	/**
	 * 消息推送配置数据
	 */
	BIN_PUSH_CONFIG
	/**
	 * 最大值
	 */
	MAX

## LUID ##
	/**
	 * 检查是否是网关设备
	 * 
	 * @param luid
	 *            设备编号
	 * @return 布尔值
	 */
	public static boolean IS_SERVER_AP(long luid)

	/**
	 * 检查是否是网关联动设备
	 * 
	 * @param luid
	 *            设备编号
	 * @return 布尔值
	 */
	public static boolean IS_SERVER_AP_LINK(long luid)

	/**
	 * 检查是否是Zigbee协调器设备
	 * 
	 * @param luid
	 *            设备编号
	 * @return 布尔值
	 */
	public static boolean IS_SERVER_COOR(long luid)


	/**
	 * 检查LUID编号是否合法
	 * 
	 * @param luid
	 *            设备编号
	 * @return 布尔值
	 */
	public static boolean IS_VALID(long luid)

	/**
	 * 检查是否是手机设备
	 * 
	 * @param luid
	 *            设备编号
	 * @return 布尔值
	 */
	public static boolean IS_MOBILE(long luid)

	/**
	 * 检查是否是以Zigbee设备
	 * 
	 * @param luid
	 *            设备编号
	 * @return 布尔值
	 */
	public static boolean IS_ZIGBEE(long luid)

	/**
	 * 检查是否是虚拟设备
	 * 
	 * @param luid
	 *            设备编号
	 * @return 布尔值
	 */
	public static boolean IS_VIRTUAL(long luid)

	/**
	 * 检查是否是蓝牙设备
	 * 
	 * @param luid
	 *            设备编号
	 * @return 布尔值
	 */
	public static boolean IS_BT(long luid)

	/**
	 * 检查是否是433/315/LoRA设备
	 * 
	 * @param luid
	 *            设备编号
	 * @return 布尔值
	 */
	public static boolean IS_RF(long luid)

	/**
	 * 检查是否是以太网/WIFI设备(具备MAC地址)
	 * 
	 * @param luid
	 *            设备编号
	 * @return 布尔值
	 */
	public static boolean IS_ETH(long luid)

	/**
	 * 检查是否是以太网/WIFI设备(不具备MAC地址)
	 * 
	 * @param luid
	 *            设备编号
	 * @return 布尔值
	 */
	public static boolean IS_ETHX(long luid)

	/**
	 * 检查是否是电力载波设备
	 * 
	 * @param luid
	 *            设备编号
	 * @return 布尔值
	 */
	public static boolean IS_PL(long luid)

	/**
	 * 检查是否是NB-IoT设备
	 * 
	 * @param luid
	 *            设备编号
	 * @return 布尔值
	 */
	public static boolean IS_NBIOT(long luid)

	/**
	 * 检查是否是BRIDGE桥接设备
	 * 
	 * @param luid
	 *            设备编号
	 * @return 布尔值
	 */
	public static boolean IS_BRIDGE(long luid)

	/**
	 * 检查是否是终端节点设备(Zigbee设备，蓝牙设备，以太网/WIFI设备，电力载波设备，433/315/LoRA设备，NB-IoT设备)
	 * 
	 * @param luid
	 *            设备编号
	 * @return 布尔值
	 */
	public static boolean IS_NODE(long luid)

## 回调接口 ##

**GatewaySearchCallback**

	/**
	 * 网关搜索回调接口
	 * 
	 * @param des
	 *            网关信息
	 * @return 布尔值，返回FALSE将停止搜索
	 */
	public boolean gatewayDetected(GATEWAY_DES des);

**XAICallback**

	/**
	 * 收到状态消息
	 * 
	 * @param from_apsn
	 *            消息来源所属网关
	 * @param from_luid
	 *            消息来源设备编号
	 * @param trigger_apsn
	 *            导致此状态消息发出的设备所属网关
	 * @param trigger_luid
	 *            导致此状态消息发出的设备编号
	 * @param timestamp
	 *            此状态改变时的时间（1970-1-1至今的秒数）
	 * @param id
	 *            状态编号
	 * @param tlvs
	 *            状态数据集
	 */
	public void messageStatus(int from_apsn, long from_luid, int trigger_apsn, long trigger_luid, long timestamp,
					int id, ArrayList<TLV> tlvs);

	/**
	 * 收到历史状态消息
	 * 
	 * @param from_apsn
	 *            消息来源所属网关
	 * @param from_luid
	 *            消息来源设备编号
	 * @param trigger_apsn
	 *            导致此状态消息发出的设备所属网关
	 * @param trigger_luid
	 *            导致此状态消息发出的设备编号
	 * @param timestamp
	 *            此状态改变时的时间（1970-1-1至今的秒数）
	 * @param id
	 *            状态编号
	 * @param tlvs
	 *            状态数据集
	 */
	public void messageStatusHistory(int from_apsn, long from_luid, int trigger_apsn, long trigger_luid, long timestamp,
					int id, ArrayList<TLV> tlvs);

	/**
	 * 收到控制消息
	 * 
	 * @param from_apsn
	 *            控制命令来源所属网关
	 * @param from_luid
	 *            控制命令来源设备编号
	 * @param timestamp
	 *            此控制命令发出时间（1970-1-1至今的秒数）
	 * @param id
	 *            命令编号
	 * @param tlvs
	 *            命令数据集
	 * @param ret_vals
	 *            返回的命令数据集
	 * @return ERRNO
	 */
	public ERRNO messageCMD(int from_apsn, long from_luid, long timestamp, int id, ArrayList<TLV> tlvs,
					ArrayList<TLV> ret_vals);

	/**
	 * 收到聊天消息
	 * 
	 * @param from_apsn
	 *            来源来源所属网关
	 * @param from_luid
	 *            消息来源设备编号
	 * @param timestamp
	 *            此消息发出时间（1970-1-1至今的秒数）
	 * @param tlvs
	 *            消息数据集
	 */
	public void messageIM(int from_apsn, long from_luid, long timestamp, ArrayList<TLV> tlvs);

	/**
	 * 设备被删除
	 * 
	 * @param apsn
	 *            设备所属网关编号
	 * @param luid
	 *            设备编号
	 */
	public void deviceRemoved(int apsn, long luid);

	/**
	 * 发现设备
	 * 
	 * @param apsn
	 *            设备所属网关编号
	 * @param luid
	 *            设备编号
	 * @param type
	 *            设备类型
	 * @param ver
	 *            设备软件版本
	 * @param des
	 *            设备文字描述
	 */
	public void deviceDetected(int apsn, long luid, int type, int ver, String des);

	/**
	 * 
	 * @param apsn
	 *            联动所属网关编号
	 * @param id
	 *            联动编号
	 * @param name
	 *            联动名称
	 * @param active
	 *            联动状态
	 * @param condition
	 *            联动条件组
	 * @param action
	 *            联动动作组
	 */
	public void linkRuleDetected(int apsn, int id, String name, boolean active, ArrayList<LINK_RULE> condition,
					ArrayList<LINK_RULE> action);

	/**
	 * 联动被删除
	 * 
	 * @param apsn
	 *            联动所属网关
	 * @param id
	 *            联动编号
	 */
	public void linkRuleRemoved(int apsn, int id);

	/**
	 * 推送配置信息
	 * 
	 * @param vendor
	 *            推送供应商
	 * @param para
	 *            推送配置信息
	 */
	public void pushParaDetected(int vendor, String para);

	/**
	 *
	 * @param apsn
	 *            推送信息所属网关编号
	 * @param enable
	 *            是否启用
	 * @param registerID
	 *            push的注册ID
	 * @param configs
	 *            配置数据
	 */
	public void pushRuleDetected(int apsn, boolean enable, String registerID, ArrayList<PushRule> configs);

	/**
	 * 网关ADMIN被设置
	 * 
	 * @param apsn
	 *            所属网关
	 * @param luid
	 *            ADMIN的LUID(只可能是手机号)
	 */
	public void adminDetected(int apsn, long luid_admin);

## 混淆配置 ##
	-dontwarn com.squareup.okhttp.**
	-keep class com.squareup.okhttp.** { *;}
	-dontwarn org.apache.commons.**
	-keep class org.apache.commons.**{*;}
	-dontwarn com.zhy.http.**
	-keep class com.zhy.http.**{*;}
	-dontwarn okhttp3.**
	-keep class okhttp3.**{*;}
	-dontwarn okio.**
	-keep class okio.**{*;}	

XAI物联网系统交流群：768813308

网站地址：[www.xai.net.cn](www.xai.net.cn)