package com.puy.xai;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.lypeer.fcpermission.FcPermissionsB;
import com.lypeer.fcpermission.impl.OnPermissionsDeniedListener;
import com.lypeer.fcpermission.impl.OnPermissionsGrantedListener;

import java.util.List;

import example.Mobile;

public class MainActivity extends AppCompatActivity {
    private EditText edit_show_message;
    private Handler mHandler;
    private EditText edit_host;
    private EditText edit_port;
    private EditText edit_username;
    private EditText edit_password;
    private Button button_register;
    private Button button_connect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();
        initData();
    }

    private void initData() {
        //为方便显示数据对EditText做设置
        edit_show_message.setMovementMethod(ScrollingMovementMethod.getInstance());
        edit_show_message.setSelection(edit_show_message.getText().length(), edit_show_message.getText().length());
        //注册帐号
        button_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,RegisterUserActivity.class));
            }
        });
        //connect按钮点击事件
        button_connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //连接服务器
                Mobile mobile = new Mobile(edit_host.getText().toString(), Integer.valueOf(edit_port.getText().toString()), edit_username.getText().toString(), edit_password.getText().toString());
                mobile.start(MainActivity.this, mHandler);
            }
        });
        //为了方便显示接收到的数据，开发者可以在XAI的XAICallback中自行设置对应数据的逻辑操作
        mHandler = new Handler() {

            @Override
            public void handleMessage(Message msg) {
                edit_show_message.append((String) msg.obj);
                edit_show_message.append("\n\n");
            }
        };
        //需要动态获取SD卡写入权限，这里XAI默认使用手机SD卡作为缓存目录，开发者也可自行设置缓存目录
        requestsForPermissions();
    }

    private void initView() {
        //获取控件
        edit_show_message = findViewById(R.id.edit_show_message);
        edit_host = findViewById(R.id.edit_host);
        edit_port = findViewById(R.id.edit_port);
        edit_username = findViewById(R.id.edit_username);
        edit_password = findViewById(R.id.edit_password);
        button_register = findViewById(R.id.button_register);
        button_connect = findViewById(R.id.button_connect);
    }

    //权限申请类
    private FcPermissionsB mFcPermissionsB;

    private void requestsForPermissions() {

        mFcPermissionsB = new FcPermissionsB.Builder(this)
                .onGrantedListener(new OnPermissionsGrantedListener() {
                    @Override
                    public void onPermissionsGranted(int requestCode, List<String> perms) {

                    }
                })
                .onDeniedListener(new OnPermissionsDeniedListener() {
                    @Override
                    public void onPermissionsDenied(int requestCode, List<String> perms) {

                    }
                })
                .positiveBtn4ReqPer(R.string.ok)
                .negativeBtn4ReqPer(R.string.cancel)
                .positiveBtn4NeverAskAgain(R.string.setting)
                .negativeBtn4NeverAskAgain(android.R.string.cancel)
                .rationale4ReqPer(getString(R.string.prompt_request_camara))
                .rationale4NeverAskAgain(getString(R.string.prompt_we_need_camera))
                .requestCode(2333)
                .build();
        //获取SD卡写入权限
        mFcPermissionsB.requestPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mFcPermissionsB.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }
}
