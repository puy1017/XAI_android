package com.puy.xai;

import android.app.Application;

import xai.sdk.api.XAI;

/**
 * Created by puy on 2018/4/23.
 */

public class App extends Application {
    private XAI mXAI;

    public App() {
    }

    public XAI getXAI() {
        return mXAI;
    }

    public void setXAI(XAI XAI) {
        mXAI = XAI;
    }

    public static final App getInstance() {
        return AppHolder.INSTANCE;
    }

    //定义的静态内部类
    private static class AppHolder {
        private static final App INSTANCE = new App();  //创建实例的地方
    }
}
