package com.puy.xai;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import xai.sdk.api.XAI;

/**
 * Created by Administrator on 2018/4/23.
 */

public class RegisterUserActivity extends Activity {
    private static final int SENDSMS = 1;
    private static final int REGISTER = 2;
    private EditText edit_host;
    private EditText edit_port;
    private EditText edit_username;
    private EditText edit_sms;
    private EditText edit_password;
    private Button button_sms;
    private Button button_register;
    private Handler mHandler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_user);

        initView();
        initData();
    }

    private void initData() {
        mHandler = new Handler(){

            @Override
            public void handleMessage(Message msg) {
                switch (msg.what){
                    case SENDSMS:
                        Toast.makeText(RegisterUserActivity.this, (String) msg.obj,Toast.LENGTH_SHORT).show();
                        break;
                    case REGISTER:
                        Toast.makeText(RegisterUserActivity.this, (String) msg.obj,Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        };
        button_sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String host = edit_host.getText().toString();
                if(host ==null || host.length()<=0){
                    Toast.makeText(RegisterUserActivity.this,"请输入IP地址",Toast.LENGTH_SHORT).show();
                    return;
                }
                final String port = edit_port.getText().toString();
                if(port ==null || port.length()<=0){
                    Toast.makeText(RegisterUserActivity.this,"请输入端口地址",Toast.LENGTH_SHORT).show();
                    return;
                }
                final String phone_num = edit_username.getText().toString();
                if(phone_num ==null || phone_num.length()<=0){
                    Toast.makeText(RegisterUserActivity.this,"请输入手机号",Toast.LENGTH_SHORT).show();
                    return;
                }
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        if(XAI.requestSMSVerifyCode(host,Integer.valueOf(port),phone_num,true)){
                            mHandler.obtainMessage(SENDSMS,"发送验证码成功").sendToTarget();
                        }else {
                            mHandler.obtainMessage(SENDSMS,"发送验证码失败").sendToTarget();
                        }
                    }
                }).start();
            }
        });
        button_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String host = edit_host.getText().toString();
                if(host ==null || host.length()<=0){
                    Toast.makeText(RegisterUserActivity.this,"请输入IP地址",Toast.LENGTH_SHORT).show();
                    return;
                }
                final String port = edit_port.getText().toString();
                if(port ==null || port.length()<=0){
                    Toast.makeText(RegisterUserActivity.this,"请输入端口地址",Toast.LENGTH_SHORT).show();
                    return;
                }
                final String phone_num = edit_username.getText().toString();
                if(phone_num ==null || phone_num.length()<=0){
                    Toast.makeText(RegisterUserActivity.this,"请输入手机号",Toast.LENGTH_SHORT).show();
                    return;
                }
                final String sms = edit_sms.getText().toString();
                if(sms ==null || sms.length()<=0){
                    Toast.makeText(RegisterUserActivity.this,"请输入验证码",Toast.LENGTH_SHORT).show();
                    return;
                }
                final String pw = edit_password.getText().toString();
                if(sms ==null || sms.length()<=0){
                    Toast.makeText(RegisterUserActivity.this,"请输入密码",Toast.LENGTH_SHORT).show();
                    return;
                }
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        String result = XAI.registerUser(host,Integer.valueOf(port),phone_num,pw,sms,true);
                        System.out.println(result);
                        mHandler.obtainMessage(REGISTER,result).sendToTarget();
                    }
                }).start();
            }
        });
    }

    private void initView() {
        edit_host = findViewById(R.id.edit_host);
        edit_port = findViewById(R.id.edit_port);
        edit_username = findViewById(R.id.edit_username);
        edit_sms = findViewById(R.id.edit_sms);
        edit_password = findViewById(R.id.edit_password);
        button_sms = findViewById(R.id.button_sms);
        button_register = findViewById(R.id.button_register);
    }
}
