package example;

import android.content.Context;
import android.os.Environment;
import android.os.Handler;

import com.puy.xai.R;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;

import xai.sdk.api.XAI;
import xai.sdk.api.XAICallback;
import xai.sdk.bean.LINK_RULE;
import xai.sdk.bean.pub.ERRNO;
import xai.sdk.bean.pub.LUID;
import xai.sdk.bean.pub.PushRule;
import xai.sdk.bean.pub.STATUS;
import xai.sdk.bean.pub.TLV;
import xai.sdk.util.pub.Convertor;
import xai.sdk.util.pub.LogFile;

public class Mobile {

    private XAI xai = null;
    private Handler handler = null;
    private String host = "47.97.198.133";
    private int port = 9001;
    private String mobile_phone = "13550075599";
    private String pwd = "123456";

    private XAICallback callback = new XAICallback() {

        @Override
        public ERRNO messageCMD(int from_apsn, long from_luid, long timestamp, int id, ArrayList<TLV> tlvs,
                                ArrayList<TLV> ret_vals) {
            LogFile.Write("cmd received from 0x%016x@0x%08x : %s", from_luid, from_apsn,
                    tlvs.get(0).getData().toString());
            handler.obtainMessage(0, String.format("cmd received from 0x%016x@0x%08x : %s", from_luid, from_apsn,
                    tlvs.get(0).getData().toString())).sendToTarget();
            return ERRNO.OK;
        }

        @Override
        public void messageStatusHistory(int from_apsn, long from_luid, int trigger_apsn, long trigger_luid,
                                         long timestamp, int id, ArrayList<TLV> tlvs) {
            timestamp *= 1000;
            Date date = new Date(timestamp);
            LogFile.Write("history status received from:0x%016x@0x%08x by:0x%016x@0x%08x,id=0x%02x,data=%s,time=%s",
                    from_luid, from_apsn, trigger_luid, trigger_apsn, id,
                    Convertor.byteArray2HexString(tlvs.get(0).getData()), date.toString());
            handler.obtainMessage(0, String.format("history status received from:0x%016x@0x%08x by:0x%016x@0x%08x,id=0x%02x,data=%s,time=%s",
                    from_luid, from_apsn, trigger_luid, trigger_apsn, id,
                    Convertor.byteArray2HexString(tlvs.get(0).getData()), date.toString())).sendToTarget();
        }

        @Override
        public void messageStatus(int from_apsn, long from_luid, int trigger_apsn, long trigger_luid, long timestamp,
                                  int id, ArrayList<TLV> tlvs) {
            LogFile.Write("status received from:0x%016x@0x%08x by:0x%016x@0x%08x,id=0x%02x,data=%s", from_luid,
                    from_apsn, trigger_luid, trigger_apsn, id, Convertor.byteArray2HexString(tlvs.get(0).getData()));
            handler.obtainMessage(0, String.format("status received from:0x%016x@0x%08x by:0x%016x@0x%08x,id=0x%02x,data=%s", from_luid,
                    from_apsn, trigger_luid, trigger_apsn, id, Convertor.byteArray2HexString(tlvs.get(0).getData()))).sendToTarget();
            if (id == STATUS.STATUS_ID_NAME) {
                try {
                    String name = new String(tlvs.get(0).getData(), "UTF-8").trim();
                    LogFile.Write("设备名:%s\n", name);
                    handler.obtainMessage(0, String.format("设备名:%s\n", name)).sendToTarget();
                } catch (UnsupportedEncodingException e) {
                    LogFile.Write(e.getMessage());
                }
            } else if (id == STATUS.STATUS_ID_CONNECTION) {
                byte[] val = tlvs.get(0).getData();
                if (val != null) {
                    if (val[0] == 0) {
                        LogFile.Write("当前设备离线\n");
                        handler.obtainMessage(0, String.format("当前设备离线\n")).sendToTarget();
                    } else if (val[0] == 1) {
                        LogFile.Write("当前设备在线\n");
                        handler.obtainMessage(0, String.format("当前设备在线\n")).sendToTarget();
                    } else if (val[0] == 2) {
                        LogFile.Write("当前设备休眠\n");
                        handler.obtainMessage(0, String.format("当前设备休眠\n")).sendToTarget();
                    }
                } else {
                    LogFile.Write("当前设备离线\n");
                    handler.obtainMessage(0, String.format("当前设备离线\n")).sendToTarget();
                }
            } else if (id == STATUS.STATUS_ID_POWER) {
                byte[] val = tlvs.get(0).getData();
                if (val != null) {
                    LogFile.Write("当前设备电量=%d%%\n", val[0] & 0xFF);
                    handler.obtainMessage(0, String.format("当前设备电量=%d%%\n", val[0] & 0xFF)).sendToTarget();
                }
            }
            if (LUID.IS_SERVER_AP(from_luid)) {
                if (id == 1) {
                    LogFile.Write("当前场景编号:%x\n", new BigInteger(tlvs.get(0).getData()).longValue());
                    handler.obtainMessage(0, String.format("当前场景编号:%x\n", new BigInteger(tlvs.get(0).getData()).longValue())).sendToTarget();
                } else if (id == 2) {
                    LogFile.Write("设备请求入网:0x%016x\n", new BigInteger(tlvs.get(0).getData()).longValue());
                    handler.obtainMessage(0, String.format("设备请求入网:0x%016x\n", new BigInteger(tlvs.get(0).getData()).longValue())).sendToTarget();
                }
            }
            return;
        }

        @Override
        public void messageIM(int from_apsn, long from_luid, long timestamp, ArrayList<TLV> tlvs) {
            LogFile.Write("im received from 0x%016x@0x%08x : %s", from_luid, from_apsn, tlvs.get(0).toString());
            handler.obtainMessage(0, String.format("im received from 0x%016x@0x%08x : %s", from_luid, from_apsn, tlvs.get(0).toString())).sendToTarget();
        }

        @Override
        public void deviceDetected(int apsn, long luid, int type, int ver, String des) {
            LogFile.Write("device detected[%s]:0x%016x@0x%08x", des, luid, apsn);
            handler.obtainMessage(0, String.format("device detected[%s]:0x%016x@0x%08x", des, luid, apsn)).sendToTarget();
        }

        @Override
        public void deviceRemoved(int apsn, long luid) {
            LogFile.Write("device removed:0x%016x@0x%08x", luid, apsn);
            handler.obtainMessage(0, String.format("device removed:0x%016x@0x%08x", luid, apsn)).sendToTarget();
        }

        @Override
        public void linkRuleDetected(int apsn, int id, String name, boolean active, ArrayList<LINK_RULE> condition,
                                     ArrayList<LINK_RULE> action) {
            LogFile.Write("link detected id:0x%02x,name=%s,active=%s", id, name, String.valueOf(active));
            handler.obtainMessage(0, String.format("link detected id:0x%02x,name=%s,active=%s", id, name, String.valueOf(active))).sendToTarget();
        }

        @Override
        public void linkRuleRemoved(int apsn, int id) {
            LogFile.Write("link removed id:0x%02x", id);
            handler.obtainMessage(0, String.format("link removed id:0x%02x", id)).sendToTarget();

        }

        @Override
        public void pushRuleDetected(int apsn, boolean enable, String registerID, ArrayList<PushRule> configs) {
            String val = String.format("push config detected: apsn=0x%08x,enable=%s,registerID=%s", apsn, enable,
                    registerID);
            handler.obtainMessage(0, String.format("push config detected: apsn=0x%08x,enable=%s,registerID=%s", apsn, enable,
                    registerID)).sendToTarget();
            for (PushRule jpush_config : configs) {
                val = val + "\n" + jpush_config.getMessage();
            }
            LogFile.Write(val);
            handler.obtainMessage(0, val).sendToTarget();
        }

        @Override
        public void adminDetected(int apsn, long luid_admin) {
            String val = String.format("admin detected: mobile=%s,apsn=0x%08x", Convertor.luidToMobile(luid_admin),
                    apsn);
            LogFile.Write(val);
            handler.obtainMessage(0, val).sendToTarget();
        }

        @Override
        public void pushParaDetected(int vendor, String para) {
            LogFile.Write("push token detected=%s", para);
            handler.obtainMessage(0, String.format("push token detected=%s", para)).sendToTarget();

        }
    };

    public Mobile(String host, int port, String mobile_phone, String pwd) {
        this.host = host;
        this.port = port;
        this.mobile_phone = mobile_phone;
        this.pwd = pwd;
    }

    public ERRNO start(Context context, Handler handler) {
        this.handler = handler;
        this.xai = new XAI(mobile_phone, callback);
        InputStream ca_file_is = context.getResources().openRawResource(R.raw.ca);
        if (xai.connect(pwd, host, port, ca_file_is, "123456", Environment.getExternalStorageDirectory() + "/GatewayShare", true) != ERRNO.OK) {
            LogFile.Write("failed to connect to server\n");
            return ERRNO.NETWORK_FAILED;
        }
        xai.sendMyDev(0x0000001, 0x00010000, false, "Wisper的手机");
        xai.sendMyDeviceConnection(true);
        xai.sendMyDeviceName("Wisper");
        xai.sendMyDevicePower(100);
        return ERRNO.OK;
    }
}
